#include "mainwrapper.h"
#include <algorithm>
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#include <string>
#include <QDebug>
#include <QMap>
#include <QMutex>

static unsigned long int next = 1;

int hrand(void) // RAND_MAX assumed to be 32767
{
    next = next * 1103515245 + 12345;
    return (unsigned int)(next/65536) % 32768;
}

void seed_rand(unsigned int seed)
{
    next = seed;
}

template< class RandomIt >
void hrandom_shuffle( RandomIt first, RandomIt last )
{
    typename std::iterator_traits<RandomIt>::difference_type i, n;
    n = last - first;
    for (i = n-1; i > 0; --i) {
        using std::swap;
        swap(first[i], first[hrand() % (i+1)]);
    }
}

MainWrapper::MainWrapper(QObject *parent)
    : QObject(parent)
    , width_(4)
{
    QStringList newGrid;

    emptyTile_ = QString::number( width_ * width_ );

    for (int i = 1; i < ( width_ * width_ + 1); i++) {
        newGrid.append( QString::number( i ) );
    }

    setGreyGrid( newGrid );
}

MainWrapper::~MainWrapper()
{
}

QStringList MainWrapper::greyGrid()
{
    return greyGrid_;
}

void MainWrapper::setGreyGrid( QStringList newGrid )
{
    if ( greyGrid_ != newGrid )
    {
        greyGrid_ = newGrid;
        emit greyGridChanged( greyGrid_ );
        emit widthChanged( width_ );
    }
}

void MainWrapper::setSeed(QString Seed)
{
    if (seed_ != Seed)
    {
        seed_ = Seed;
        emit seedChanged(seed_);
    }
}

QString MainWrapper::seed()
{
    return seed_;
}

QString MainWrapper::emptyTile()
{
    return emptyTile_;
}

void MainWrapper::setEmptyTile( QString EmptyTile )
{
    if ( emptyTile_ != EmptyTile )
    {
        emptyTile_ = EmptyTile;
        emit emptyTileChanged( emptyTile_ );
    }
}

int MainWrapper::width()
{
    return width_;
}

void MainWrapper::setWidth(int newWidth)
{
    if ( width_ != newWidth )
    {
        width_  = newWidth;
        QStringList newGrid;

        setEmptyTile( QString::number( width_ * width_ ) );
        for (int i = 1; i < ( width_ * width_ + 1); i++) {
            newGrid.append( QString::number( i ) );
        }

        setGreyGrid( newGrid );
    }
}

void MainWrapper::Scramble()
{
    unsigned seed = std::time(0);
    setSeed( QString::number( seed ) );
    seed_rand( seed );
    hrandom_shuffle( greyGrid_.begin(), greyGrid_.end() );
    checkSolvable();
}

void MainWrapper::SetSeed(QString inputSeed)
{
    setSeed( inputSeed );
    unsigned seed = inputSeed.toUInt();
    seed_rand( seed );
    hrandom_shuffle( greyGrid_.begin(), greyGrid_.end() );
    checkSolvable();
}

void MainWrapper::TileClicked( QString index )
{
    QString blankTile = QString::number( width_ * width_ );
    if ( index == blankTile )
        return;

    QMap< QString, QPair< int, int > > tileMap;
    int counter = 0;
    for ( int y = 0; y < width_; y++ )
    {
        for (int x = 0; x < width_; x++ )
        {
            tileMap[ greyGrid_[ counter ] ] = QPair< int, int >( x, y );
            counter++;
        }
    }


    if ( tileMap[ blankTile ].first == tileMap[ index ].first )
    {
        QPair< int, int > finalDest = tileMap[ index ];
        if ( tileMap[ blankTile ].second > tileMap[ index ].second )
        {
            for ( int shiftedY = tileMap[ blankTile ].second - 1; shiftedY >= tileMap[ index ].second; shiftedY-- )
            {
                tileMap[ tileMap.key( QPair< int, int > ( tileMap[ blankTile ].first, shiftedY ) ) ] = QPair< int, int > ( tileMap[ blankTile ].first, shiftedY + 1 );
            }
        }
        else
        {
            for ( int shiftedY = tileMap[ blankTile ].second + 1; shiftedY <= tileMap[ index ].second; shiftedY++ )
            {
                tileMap[ tileMap.key( QPair< int, int > ( tileMap[ blankTile ].first, shiftedY ) ) ] = QPair< int, int > ( tileMap[ blankTile ].first, shiftedY - 1 );
            }
        }
        tileMap[ blankTile ] = finalDest;
    }

    if ( tileMap[ blankTile ].second == tileMap[ index ].second )
    {
        QPair< int, int > finalDest = tileMap[ index ];
        if ( tileMap[ blankTile ].first > tileMap[ index ].first )
        {
            for ( int shiftedX = tileMap[ blankTile ].first - 1; shiftedX >= tileMap[ index ].first; shiftedX-- )
            {
                tileMap[ tileMap.key( QPair< int, int > ( shiftedX, tileMap[ blankTile ].second ) ) ] = QPair< int, int > ( shiftedX + 1, tileMap[ blankTile ].second );
            }
        }
        else
        {
            for ( int shiftedX = tileMap[ blankTile ].first + 1; shiftedX <= tileMap[ index ].first; shiftedX++ )
            {
                tileMap[ tileMap.key( QPair< int, int > ( shiftedX, tileMap[ blankTile ].second ) ) ] = QPair< int, int > ( shiftedX - 1, tileMap[ blankTile ].second );
            }
        }
        tileMap[ blankTile ] = finalDest;
    }

    QStringList newList;
    QStringList winList;
    counter = 1;
    for ( int y = 0; y < width_; y++ )
    {
        for (int x = 0; x < width_; x++ )
        {
            newList.append( tileMap.key( QPair< int, int >( x, y ) ) );
            winList.append( QString::number( counter ) );
            counter++;
        }
    }

    if ( newList == winList )
        emit puzzleCompleted();

    setGreyGrid( newList );
}

void MainWrapper::FinishTime( int totalTime )
{
    QString ( seed_ + "," + QString::number( totalTime) );
}

void MainWrapper::checkSolvable()
{
    int inversions = 0;
    bool isEven = true;
    bool isEvenRow = true;
    int evenCounter = 0;
    for (int i = 0; i < greyGrid_.size() - 1; i++)
    {
        int checkInt = QString(greyGrid_[ i ]).toInt();
        int cI = 0;

        if ( evenCounter == width_ )
        {
            isEvenRow = !isEvenRow;
            evenCounter = 0;
        }

        if ( checkInt != width_ * width_ )
        {
            for (int j = i + 1; j < greyGrid_.size(); j++ )
            {
                if ( checkInt > QString( greyGrid_[j]).toInt() )
                {
                    cI++;
                    inversions++;
                }
            }
        }
        else
        {
            isEven = isEvenRow;
        }

        evenCounter++;
    }

    bool ret = true;

    if ( ( width_ % 2) == 0 )
    {
        if ( ( isEven ) && ( inversions % 2 == 0) )
        {
            ret = false;
        }

        if ( (!isEven) && ( inversions % 2 != 0) )
        {
            ret = false;
        }
    }
    else
    {
        ret = ( ( inversions % 2 ) == 0 );
    }


    if (ret)
    {

        QString pid;
        foreach (const QString & Tile, greyGrid_)
                pid.append( Tile + "," );

        pid.chop( 1 );

        emit greyGridChanged( greyGrid_ );
    }
    else
    {
        unsigned seed = seed_.toUInt();
        seed++;

        SetSeed( QString::number( seed ) );
    }
}

void MainWrapper::TileSwiped(QString Tile, int OldX, int X, int OldY, int Y)
{
    if ( ( abs ( X - OldX) < 20 ) && ( abs( Y - OldY) < 20 ))
    {
        TileClicked( Tile );
    }
    else
    {
        if ( abs ( X - OldX) > abs( Y - OldY) )
        {
            //qDebug() << "Tile: " << Tile << " X Swipe";
            TileClicked( Tile );
        }
        else
        {
            //qDebug() << "Tile: " << Tile << " Y Swipe";
            TileClicked( Tile );
        }
    }
}

void MainWrapper::UpgradeYa(QString input)
{
    if ( input.toInt() > 2 )
        setWidth( input.toInt() );
    else
        setWidth( 4 );
}

void MainWrapper::LoadSave(QStringList InputGrid, QString Seed, int Width)
{
    qDebug() << InputGrid << Seed << Width;
    greyGrid_ = InputGrid;
    seed_ = Seed;
    width_ = Width;
    emptyTile_ = QString::number( width_ * width_ );

    emit emptyTileChanged( emptyTile_ );
    emit widthChanged( width_ );
    emit seedChanged( seed_ );
    emit greyGridChanged( greyGrid_ );
}
