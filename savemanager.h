#ifndef SAVEMANAGER_H
#define SAVEMANAGER_H

#include <QObject>

class SaveManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool hasExisting READ hasExisting WRITE setHasExisting NOTIFY hasExistingChanged)
public:
    explicit SaveManager(QObject *parent = 0);
    void ReadSaveData();

    bool hasExisting();
    void setHasExisting( bool );

signals:
    void hasExistingChanged( bool );

public slots:
    bool WriteData( QStringList Puzzle, QString Seed, int TimeMS, int TimeS, int TimeM, int PuzzleWidth, int FontSize );
    int GetFontSize();
    QStringList GetPuzzle();
    QString GetSeed();
    int GetPuzzleWidth();
    int GetMSTime();
    int GetSTime();
    int GetMTime();

private:
    bool hasExisting_;
    QString writableLocation_;
    QStringList savedGame_;
};

#endif // SAVEMANAGER_H
