#include "savemanager.h"
#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDebug>

SaveManager::SaveManager(QObject *parent)
    : QObject(parent)
    , hasExisting_( false )
    , savedGame_( QStringList() )
{
    writableLocation_= QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    QDir writableDir(writableLocation_);
    if (!writableDir.exists())
        QDir().mkdir(writableLocation_);

    QFile newFile(QString(writableLocation_ + QDir::separator() + "15puzzle.ini"));
    if (newFile.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        newFile.close();
    }

    ReadSaveData();
}

bool SaveManager::hasExisting()
{
    return hasExisting_;
}

void SaveManager::setHasExisting( bool HasExisting )
{
    if ( hasExisting_ != HasExisting )
    {
        hasExisting_ = HasExisting;
        emit hasExistingChanged( hasExisting_ );
    }
}

void SaveManager::ReadSaveData()
{
    QFile saveFile(QString(writableLocation_ + QDir::separator() + "15puzzle.ini"));
    if (saveFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QStringList fileContents = QString( saveFile.readAll() ).split("\n");


        if ( fileContents.size() > 6) {
            setHasExisting( true );
            savedGame_ = fileContents;
        }
    }

    saveFile.close();
}

bool SaveManager::WriteData(QStringList Puzzle, QString Seed, int TimeMS, int TimeS, int TimeM, int PuzzleWidth, int FontSize)
{
    QString puzzle;
    foreach( const QString & tile, Puzzle )
        puzzle.append( tile + "," );
    puzzle.chop( 1 );

    QFile mFile( QString( QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation ) + QDir::separator() + "15puzzle.ini"));
    if ( mFile.open( QIODevice::WriteOnly | QIODevice::Text) )
    {
        QTextStream mStream( & mFile );
        mStream << puzzle << "\n" << Seed << "\n" << TimeMS << "\n" << TimeS << "\n" << TimeM << "\n"<< PuzzleWidth << "\n" << FontSize;
        mFile.close();
    }

    ReadSaveData();

    return true;
}

int SaveManager::GetFontSize()
{
    return QString( savedGame_[6]).toInt();
}

QStringList SaveManager::GetPuzzle()
{
    QStringList puzzle = QString( savedGame_[0] ).split(",");
    return puzzle;
}

QString SaveManager::GetSeed()
{
    return savedGame_[1];
}

int SaveManager::GetPuzzleWidth()
{
    return QString( savedGame_[5]).toInt();
}

int SaveManager::GetMSTime()
{
    return QString( savedGame_[2]).toInt();
}

int SaveManager::GetSTime()
{
    return QString( savedGame_[3]).toInt();
}

int SaveManager::GetMTime()
{
    return QString( savedGame_[4]).toInt();
}
