#ifndef MAINWRAPPER_H
#define MAINWRAPPER_H

#include <QObject>
#include <QMap>

class MainWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList greyGrid READ greyGrid WRITE setGreyGrid NOTIFY greyGridChanged)
    Q_PROPERTY(QString seed READ seed WRITE setSeed NOTIFY seedChanged)
    Q_PROPERTY(QString emptyTile READ emptyTile WRITE setEmptyTile NOTIFY emptyTileChanged)
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)

public:
    explicit MainWrapper( QObject *parent = 0 );
    ~MainWrapper();

    QStringList greyGrid();
    void setGreyGrid( QStringList );


    QString seed();
    void setSeed(QString);

    QString emptyTile();
    void setEmptyTile( QString );

    int width();
    void setWidth( int );

signals:
    void greyGridChanged( QStringList );
    void puzzleCompleted();
    void seedChanged(QString);
    void emptyTileChanged( QString );
    void widthChanged( int );


public slots:
    void TileClicked( QString );
    void Scramble();
    void SetSeed( QString );
    void FinishTime( int );
    void TileSwiped( QString, int, int, int, int );
    void UpgradeYa( QString );
    void LoadSave( QStringList InputGrid, QString Seed, int Width );

private:
    void checkSolvable();
    QStringList greyGrid_;
    QString seed_, emptyTile_;
    int width_;
};

#endif // MAINWRAPPER_H
