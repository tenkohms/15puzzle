import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1

import SaveManager 1.0

Window {
    visible: true
    width: 480
    height: 800

    SaveMan {
        id: saveman
    }

    Loader {
        anchors.fill: parent
        id: loader
        source: "qrc:///menupage.qml"
    }
}
