import QtQuick 2.6
import QtQuick.Controls 2.1
import MainWrapper 1.0

Item {
    anchors.fill: parent
    property int timeMS: 0
    property int timeS: 0
    property int timeM: 0
    property int gestureX: 0
    property int gestureY: 0
    property int gridWidth: 0
    property int fontsize: 42

    function gameDone() {
        gameTimer.stop()
        mainwrapper.FinishTime( ( timeM * 60 * 1000 ) + ( timeS * 1000 ) + timeMS )
        grid.visible = false
        scoredialog.visible = true
    }

    function updateTime() {
        if ( timeMS < 9 )
        {
            timeMS = timeMS + 1;
        }
        else
        {
            timeMS = 0;
            if ( timeS < 59 )
            {
                timeS = timeS + 1;
            }
            else
            {
                timeS = 0;
                timeM = timeM + 1;
            }
        }
    }

    function begin() {
        timeMS = 0
        timeS = 0
        timeM = 0
        backButton.anchors.bottom = undefined
        backButton.anchors.top = bg.bottom
        backButton.anchors.topMargin = 20
        pauseButton.visible = true
        clockText.visible = true
        mainwrapper.Scramble()
        gameTimer.start()
    }

    WrapperMan
    {
        id: mainwrapper
    }

    Connections {
        target: mainwrapper
        onPuzzleCompleted: gameDone()
    }

    Timer {
        id: gameTimer
        interval: 100
        running: false
        repeat: true
        onTriggered: updateTime()
    }
    Rectangle {
        anchors.fill: parent
        color: "white"
    }

    Text {
        id: clockText
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: bg.top
        anchors.bottomMargin: 10
        width: grid.width
        text: ( timeM > 9 ? timeM : "0" + timeM) + ":" + ( timeS > 9 ? timeS : "0" + timeS) + "." + timeMS
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
        horizontalAlignment: Text.AlignHCenter
        color: "black"
        visible: false
    }

    Text {
        id: pid
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Rectangle {
        id: bg
        anchors.horizontalCenter: grid.horizontalCenter
        anchors.verticalCenter: grid.verticalCenter
        radius: height / 25
        width: parent.width * .85
        height: width
        color: "black"
    }

    Component {
        id: mainwrapperdelegate
        Item {
            Column {
                Rectangle {
                    clip: true
                    height: grid.height / mainwrapper.width
                    width: grid.width / mainwrapper.width
                    color: "grey"
                    anchors.horizontalCenter: parent.horizontalCenter
                    Rectangle {
                        clip: true
                        radius: height / 25
                        anchors.centerIn: parent
                        width: parent.width * .9
                        height: parent.height * .9
                        color: {
                            ( modelData == mainwrapper.emptyTile ? "grey" : "lightblue" )
                        }
                        Text {
                            visible: {
                                ( modelData == mainwrapper.emptyTile ? false : true )
                            }

                            anchors.centerIn: parent
                            text: modelData
                            font.pixelSize: fontsize
                            color: "black"
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if ( gameTimer.running == true){
                                    mainwrapper.TileClicked( modelData )
                                }
                            }
                        }

                        MultiPointTouchArea {
                            anchors.fill: parent
                            touchPoints: [
                                TouchPoint { id: point1 }
                            ]
                            onPressed: {
                                gestureX = point1.x
                                gestureY = point1.y
                            }

                            onReleased: {
                                if ( gameTimer.running == true ){
                                    mainwrapper.TileSwiped( modelData, gestureX, point1.x, gestureY, point1.y )
                                }
                            }

                        }
                    } //inner rect
                } //Outer Rectangle
            } //column
        } //Item
    }

    GridView {
        clip: true
        interactive: false
        anchors.centerIn: parent
        width: parent.width * .8
        height: width
        id: grid
        cellWidth: grid.width / mainwrapper.width; cellHeight: grid.height / mainwrapper.width
        model:mainwrapper.greyGrid
        delegate: mainwrapperdelegate
    } //gridview

    Rectangle {
        id: seeddialog
        height: grid.height
        width: grid.width
        color: "gray"
        anchors.centerIn: parent
        visible: false
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Seed Select"
            anchors.bottom: seedtextfield.top
            anchors.bottomMargin: 15
            width: parent.width * .8
            horizontalAlignment: Text.AlignHCenter
            fontSizeMode: Text.HorizontalFit; minimumPixelSize: 10; font.pixelSize: 72
        }

        TextField {
            id: seedtextfield
            validator: RegExpValidator {
                    regExp: /[0-9]+/
                }
            horizontalAlignment: Text.AlignHCenter
            anchors.centerIn: parent

        }
        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: seedtextfield.bottom
            anchors.topMargin: 10
            text: "Play"
            onClicked:
            {
                mainwrapper.SetSeed(seedtextfield.text)
                timeMS = 0
                timeS = 0
                timeM = 0
                clockText.visible = true
                seeddialog.visible = false
                backButton.anchors.bottom = undefined
                backButton.anchors.top = bg.bottom
                backButton.anchors.topMargin = 20
                pauseButton.visible = true
                grid.visible = true
                gameTimer.start()
            }
        }
    }

    Rectangle {
        id: scoredialog
        visible: false
        height: grid.height
        width: grid.width
        color: "grey"
        anchors.centerIn: parent

        Text {
            id: awesome
            anchors.horizontalCenter: parent.horizontalCenter
            text: "AWESOME!"
            color: "black"
            anchors.top: parent.top
            anchors.topMargin: 20
            horizontalAlignment: Text.AlignHCenter
            width: parent.width * .8
        }

        Text {
            id: scoreseed
            anchors.top: awesome.bottom
            text: "Seed: " + mainwrapper.seed
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 15
            horizontalAlignment: Text.AlignHCenter
            width: parent.width * .8
        }

        Text {
            id: scorewidth
            anchors.top: scoreseed.bottom
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Width: " + mainwrapper.width
        }

        Button {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: scorewidth.bottom
            anchors.topMargin: 10
            text: "Done"
            onClicked:
            {
                seedtextfield.text = ""
                beginbutton.visible = true
                playseedbutton.visible = true
                grid.visible = true
                scoredialog.visible = false
                clockText.visible = false
                settingsButton.visible = true
                backButton.anchors.top = undefined
                backButton.anchors.bottom = grid.top
                backButton.anchors.bottomMargin = 20
                pauseButton.visible = false
            }
        }
    }

    Rectangle {
        clip: true
        id: settingsDialog
        height: grid.height
        width: grid.width
        color: "grey"
        anchors.centerIn: parent
        visible: false

        Label {
            id: settingslabel
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Settings"
        }

        Label {
            id: widthlabel
            text: "Width: "
            anchors.verticalCenter: widthtextfield.verticalCenter
            anchors.right: widthtextfield.left
            anchors.rightMargin: 10

        }

        TextField {
            id: widthtextfield
            anchors.top: settingslabel.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            width: widthlabel.width
            text: "4"
            validator: RegExpValidator {
                    regExp: /[0-9]+/
                }
        }

        Button {
            id: setbutton
            anchors.left: widthtextfield.right
            anchors.leftMargin: 10
            anchors.verticalCenter: widthtextfield.verticalCenter
            text: "Set"
            onClicked: {
                mainwrapper.UpgradeYa( widthtextfield.text )
            }
        }

        Column {
            id: exampleTile
            anchors.top: widthtextfield.bottom
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            Rectangle {
                clip: true
                height: grid.height / mainwrapper.width
                width: grid.width / mainwrapper.width
                color: "grey"
                anchors.horizontalCenter: parent.horizontalCenter
                Rectangle {
                    clip: true
                    radius: height / 25
                    anchors.centerIn: parent
                    width: parent.width * .9
                    height: parent.height * .9
                    color: "lightblue"
                    Text {
                        anchors.centerIn: parent
                        text: mainwrapper.emptyTile
                        font.pixelSize: fontsize
                        color: "black"
                    }
                } //inner rect
            } //Outer Rectangle
        } //column

        Button {
            id: fontplusbutton
            anchors.top: exampleTile.bottom
            anchors.topMargin: 15
            anchors.right: setbutton.right
            text: "Font++"
            onClicked: {
                fontsize++;
            }
        }

        Button {
            id: fontminusbutton
            anchors.top: exampleTile.bottom
            anchors.topMargin: 15
            anchors.left: widthlabel.left
            text: "Font--"
            onClicked: {
                fontsize--;
            }
        }

        Button {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Done"
            onClicked: {
                settingsDialog.visible = false
                settingsButton.visible = true
            }
        }

    }

    Rectangle {
        clip: true
        id: pauseDialog
        height: grid.height
        width: grid.width
        color: "grey"
        anchors.centerIn: parent
        visible: false

        Label {
            id: pauseLabel
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Paused"
        }

        Button {
            id: resumeButton
            anchors.top: pauseLabel.bottom
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Resume"
            onClicked: {
                pauseDialog.visible = false
                grid.visible = true
                pauseButton.visible = true
                gameTimer.running = true
            }
        }

        Button {
            id: saveButton
            anchors.top: resumeButton.bottom
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Save and Back"
            onClicked: {
                if ( saveman.WriteData( mainwrapper.greyGrid, mainwrapper.seed, timeMS, timeS, timeM, mainwrapper.width, fontsize ) )
                {
                    gameTimer.stop
                    loader.source = "qrc:///menupage.qml"
                }
            }
        }
    }

    Button {
        id: pauseButton
        anchors.top: bg.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.topMargin: 20
        visible: false
        text: "Pause"
        onClicked: {
            gameTimer.running = false
            pauseDialog.visible = true
            grid.visible = false
            pauseButton.visible = false
        }
    }

    Button {
        id: beginbutton
        anchors.top: bg.bottom
        anchors.topMargin: 20
        anchors.right: bg.right
        text: "Scramble!"
        onClicked: {
            playseedbutton.visible = false
            beginbutton.visible = false
            settingsButton.visible = false
            begin();
        }
    }

    Button {
        id: playseedbutton
        anchors.top: bg.bottom
        anchors.topMargin: 20
        anchors.left: bg.left
        text: "Play Seed"
        onClicked: {
            playseedbutton.visible = false
            seeddialog.visible = true
            grid.visible = false
            beginbutton.visible = false
            settingsButton.visible = false
        }
    }

    Button {
        id: settingsButton
        anchors.top: bg.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Settings"
        onClicked: {
            settingsDialog.visible = true
            settingsButton.visible = false
        }
    }

    Button {
        id: backButton
        anchors.left: bg.left
        anchors.bottom: bg.top
        anchors.bottomMargin: 20
        text: "Back"
        onClicked: {
            if ( ( seeddialog.visible == true ) || ( settingsDialog.visible == true ) )
            {
                seeddialog.visible = false
                settingsDialog.visible = false
                grid.visible = true
                seedtextfield.text = ""
                beginbutton.visible = true
                playseedbutton.visible = true
                grid.visible = true
                scoredialog.visible = false
                clockText.visible = false
                settingsButton.visible = true
                backButton.anchors.top = undefined
                backButton.anchors.bottom = grid.top
                backButton.anchors.bottomMargin = 20
            }
            else {
                loader.source = "qrc:///menupage.qml"
            }
        }
    }
}
