import QtQuick 2.0
import QtQuick.Controls 2.1

Item {
    Rectangle {
        anchors.fill: parent
        color: "grey"

        Text {
            id: awesome
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Challenge!"
            color: "black"
            anchors.top: parent.top
            anchors.topMargin: 20
            horizontalAlignment: Text.AlignHCenter
            width: parent.width * .8
            fontSizeMode: Text.HorizontalFit; minimumPixelSize: 10; font.pixelSize: 72
        }

        Text {
            id: serverlabel
            anchors.top: awesome.bottom
            anchors.topMargin: 15
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Enter Server URL"
        }

        TextField {
            id: serverurl
            anchors.top: serverlabel.bottom
            anchors.bottomMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * .75
        }

        Button {
            id: connectpushbutton
            anchors.top: serverurl.bottom
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Connect"
            onClicked: {
                socket.active = !socket.active
            }
        }
    }


}
