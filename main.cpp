#include <QGuiApplication>
#include <QtQml>
#include <QQmlApplicationEngine>

#include "mainwrapper.h"
#include "savemanager.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    qmlRegisterType<MainWrapper>("MainWrapper", 1, 0, "WrapperMan");
    qmlRegisterType<SaveManager>("SaveManager", 1, 0, "SaveMan");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
