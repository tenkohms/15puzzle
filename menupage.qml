import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.1

Item {
    id: menupage
    anchors.fill: parent

    Rectangle {
        id: titleborder
        anchors.fill: title
        color: "black"
    }

    Text {
        id: title
        anchors.top: parent.top
        anchors.topMargin: 20
        width: parent.width * .8
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        anchors.horizontalCenter: parent.horizontalCenter
        text: "15 Puzzle"
        color: "white"
    }

    Image {
        id: blockimage
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: titleborder.bottom
        anchors.topMargin: 15
        width: titleborder.width
        height: width
        fillMode: Image.PreserveAspectFit
        source:"qrc:/images/blockImage.jpg"
    }

    Rectangle {
        id: freeplaybutton
        anchors.top: blockimage.bottom
        anchors.topMargin: 15
        width: titleborder.width * .6
        height:titleborder.height
        anchors.horizontalCenter: parent.horizontalCenter
        color: "black"
        Text {
            anchors.centerIn: parent
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.horizontalCenter: parent.horizontalCenter
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
            text: "Free Play"
            color: "white"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: loader.source = "qrc:///FreePlay.qml"
        }
    }

    Rectangle {
        id: continueButton
        anchors.top: freeplaybutton.bottom
        anchors.topMargin: 15
        width: titleborder.width * .6
        height:titleborder.height
        anchors.horizontalCenter: parent.horizontalCenter
        color: {
            if ( saveman.hasExisting === true )
            {
                "black"
            }
            else
            {
                "grey"
            }
        }

        Text {
            anchors.centerIn: parent
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.horizontalCenter: parent.horizontalCenter
            fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 72
            text: "Continue"
            color: "white"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if ( saveman.hasExisting === true )
                {
                    loader.source = "qrc:///ResumeGame.qml"
                }
            }
        }
    }
}
