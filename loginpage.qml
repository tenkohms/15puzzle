import QtQuick 2.6
import QtQuick.Controls 2.1

Item {
    anchors.fill: parent
    Rectangle {
        anchors.centerIn: parent
        width: parent.width * .9
        height: width
        color: "black"
        StackView {
            anchors.centerIn: parent
            width: parent.width * .9
            height: parent.height * .9
            initialItem: "qrc:///challengeserverselect.qml"
        }
    }

}
